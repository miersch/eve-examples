Currently only working with conda environments!
Example on how to use the [submitit tool](https://github.com/facebookincubator/submitit) on Eve.

Submitit can be used to submit to SLURM on EVE from within a Python script. This can help with fast prototyping and easily splitting up jobs into array jobs.
The python script can run on the frontend, it uses minimal resources. However, if submitit should also be used to monitor the jobs, the python script has to run until all jobs are done. This can be achieved by running it headless.

## Installation
Use a conda environment with all necessary packages, including submitit (`pip install submitit`) installed. 
### this is not working
```
module purge # clean environment
ml foss/2022a PyTorch # load PyTorch form the toolchain
cd eve-examples/environments # move to directory of your environment
python3 -m venv submitit-test # make torch test environment
source submitit-test/bin/activate # activate the environment
pip install gpytorch submitit # install additional software into the envrionment
```

## Running headless python scripts on eve

To run a headless (python) script on EVE use

`nohup python your_script.py &`
The output tf the script will be written to nohub.out in the same directory. To redirect the output use

`nohup python your_script.py > output.log 2>&1 &`
- nohup: Prevents the script from being terminated when the terminal is closed.
- python your_script.py: Executes your Python script.
- > output.log: Redirects the script's standard output to the output.log file.
- 2>&1: Redirects the script's standard error to the same location as standard output.
- &: Runs the command in the background.