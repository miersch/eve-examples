import torch

def main():
    print(f'cuda available: {torch.cuda.is_available()}')
    available_gpus = [torch.cuda.device(i) for i in range(torch.cuda.device_count())]
    if len(available_gpus) !=0:
        print(f'available gpus: {available_gpus}')
        current_device = torch.cuda.current_device()
        print(f'torch current device is number: {current_device} that is {torch.cuda.device(current_device)} named {torch.cuda.get_device_name(current_device)}')
    else:
        print('no gpu found')
    print('basic tensor conctaination')

    x_1 = torch.randn(2, 5)
    y_1 = torch.randn(3, 5)
    z_1 = torch.cat([x_1, y_1])
    print(z_1)

if __name__ == '__main__':
    main()