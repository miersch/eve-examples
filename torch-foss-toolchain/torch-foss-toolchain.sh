#!/bin/bash
 
#SBATCH --job-name=torch-foss-toolchain
#SBATCH --output=/work/%u/logs/%x-%j.log
#SBATCH --time=0-00:05:00

#SBATCH --mem-per-cpu=1G
#SBATCH -G 1

# Load modules here
# load the toolchain
module purge
ml foss/2022a PyTorch

# Execute your app
srun python torch-foss-toolchain.py
