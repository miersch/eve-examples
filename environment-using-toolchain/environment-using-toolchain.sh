#!/bin/bash
 
#SBATCH --job-name=environment-using-toolchain
#SBATCH --output=/work/%u/logs/%x-%j.log
#SBATCH --time=0-00:05:00

#SBATCH --mem-per-cpu=1G
#SBATCH -G 1

# Load modules here
# load the toolchain
module purge
ml foss/2022a PyTorch
source ../environments/torch-test/bin/activate # activate the environment

# Execute your app
srun python environment-using-toolchain.py

