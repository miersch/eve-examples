Example on how to add packages to your environment while still using pytorch from the toolchain

## Installation
```
module purge # clean environment
ml foss/2022a PyTorch # load PyTorch form the toolchain
cd /eve-examples/environments # move to directory of your environment
python3 -m venv torch-test make torch test environment
source torch-test/bin/activate # activate the environment
pip install gpytorch # install additional software into the envrionment
```
