import torch
import gpytorch
import time
import matplotlib.pyplot as plt

# Define a more complex GP Model with ARD for multiple features
class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.RBFKernel(ard_num_dims=train_x.size(1))
        )

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

# Function to train the model and measure time
def train_gp(train_x, train_y, device):
    train_x = train_x.to(device)
    train_y = train_y.to(device)
    
    # Initialize likelihood and model
    likelihood = gpytorch.likelihoods.GaussianLikelihood().to(device)
    model = ExactGPModel(train_x, train_y, likelihood).to(device)
    
    # Find optimal model hyperparameters
    model.train()
    likelihood.train()
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters
    
    # "Loss" for GPs - the marginal log likelihood
    mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)
    
    # Training loop
    training_iter = 50
    start_time = time.time()
    
    for i in range(training_iter):
        optimizer.zero_grad()
        output = model(train_x)
        loss = -mll(output, train_y)
        loss.backward()
        optimizer.step()
    
    end_time = time.time()
    training_time = end_time - start_time
    return training_time

# Function to run experiments for different sample sizes
def run_experiments_sample_size(sample_sizes, num_features):
    cpu_times = []
    gpu_times = []
    
    for num_data_points in sample_sizes:
        print(num_data_points)
        train_x = torch.randn(num_data_points, num_features)
        train_y = torch.sin(train_x[:, 0] * (2 * torch.pi)) + torch.randn(num_data_points) * 0.2
        
        # Train on CPU
        cpu_device = torch.device("cpu")
        cpu_time = train_gp(train_x, train_y, cpu_device)
        cpu_times.append(cpu_time)
        
        # Train on GPU
        if torch.cuda.is_available():
            gpu_device = torch.device("cuda")
            gpu_time = train_gp(train_x, train_y, gpu_device)
            gpu_times.append(gpu_time)
        else:
            print("CUDA is not available. Skipping GPU training.")
            gpu_times.append(None)
    
    return cpu_times, gpu_times


def run_experiments_feature_size(sample_sizes, num_features):
    cpu_times = []
    gpu_times = []
    
    for f in num_features:
        num_data_points = sample_sizes
        print(f)
        train_x = torch.randn(num_data_points, f)
        train_y = torch.sin(train_x[:, 0] * (2 * torch.pi)) + torch.randn(num_data_points) * 0.2
        
        # Train on CPU
        cpu_device = torch.device("cpu")
        cpu_time = train_gp(train_x, train_y, cpu_device)
        cpu_times.append(cpu_time)
        
        # Train on GPU
        if torch.cuda.is_available():
            gpu_device = torch.device("cuda")
            gpu_time = train_gp(train_x, train_y, gpu_device)
            gpu_times.append(gpu_time)
        else:
            print("CUDA is not available. Skipping GPU training.")
            gpu_times.append(None)
    
    return cpu_times, gpu_times

# Define sample sizes and number of features
sample_sizes = [500, 1000, 1500, 2000,2500]
# sample_sizes = [100,200,500,1000]
num_features = 10

# Run experiments
cpu_times, gpu_times = run_experiments_sample_size(sample_sizes, num_features)

plt.rcParams.update({'font.size': 22})

# Plot the results
plt.figure(figsize=(10, 6))
plt.plot(sample_sizes, cpu_times, label="CPU", marker='o')
if None not in gpu_times:
    plt.plot(sample_sizes, gpu_times, label="GPU", marker='o')
plt.xlabel("Sample Size")
plt.ylabel("Training Time (seconds)")
plt.title("Training Time vs Sample Size for CPU and GPU")
plt.legend()
plt.grid(False)
plt.savefig('training_time_sample_size.png')


# Define sample sizes and number of features
sample_sizes = 1000
# sample_sizes = [100,200,500,1000]
num_features = [2,4,6,8,10,12,14,16,18,20]

# Run experiments
cpu_times, gpu_times = run_experiments_feature_size(sample_sizes, num_features)

# Plot the results
plt.figure(figsize=(10, 6))
plt.plot(num_features, cpu_times, label="CPU", marker='o')
if None not in gpu_times:
    plt.plot(num_features, gpu_times, label="GPU", marker='o')
plt.xlabel("Number of Features")
plt.ylabel("Training Time (seconds)")
plt.title("Training Time vs Number of Features for CPU and GPU")
plt.legend()
plt.grid(False)
plt.savefig('training_time_feature_size.png')