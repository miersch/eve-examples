import torch
import gpytorch
import time
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Define a more complex GP Model with ARD for multiple features



class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.RBFKernel(ard_num_dims=train_x.size(1))
        )

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

# Function to train the model and measure time
def train_gp(train_x, train_y, device):
    train_x = train_x.to(device)
    train_y = train_y.to(device)
    
    # Initialize likelihood and model
    likelihood = gpytorch.likelihoods.GaussianLikelihood().to(device)
    model = ExactGPModel(train_x, train_y, likelihood).to(device)
    
    # Find optimal model hyperparameters
    model.train()
    likelihood.train()
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters
    
    # "Loss" for GPs - the marginal log likelihood
    mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)
    
    # Training loop
    training_iter = 50
    start_time = time.time()
    
    for i in range(training_iter):
        optimizer.zero_grad()
        output = model(train_x)
        loss = -mll(output, train_y)
        loss.backward()
        optimizer.step()
    
    end_time = time.time()
    training_time = end_time - start_time
    return training_time

# Function to run experiments for different sample sizes and feature sizes
def run_experiments(sample_sizes, feature_sizes):
    cpu_times = np.zeros((len(sample_sizes), len(feature_sizes)))
    gpu_times = np.zeros((len(sample_sizes), len(feature_sizes)))
    
    for i, num_data_points in enumerate(sample_sizes):
        for j, num_features in enumerate(feature_sizes):
            print(f'num_samples: {num_data_points} | num_features: {num_features}')
            train_x = torch.randn(num_data_points, num_features)
            train_y = torch.sin(train_x[:, 0] * (2 * torch.pi)) + torch.randn(num_data_points) * 0.2
            
            # Train on CPU
            cpu_device = torch.device("cpu")
            cpu_time = train_gp(train_x, train_y, cpu_device)
            cpu_times[i, j] = cpu_time
            
            # Train on GPU
            if torch.cuda.is_available():
                gpu_device = torch.device("cuda")
                gpu_time = train_gp(train_x, train_y, gpu_device)
                gpu_times[i, j] = gpu_time
            else:
                print("CUDA is not available. Skipping GPU training.")
                gpu_times[i, j] = np.nan
    
    return cpu_times, gpu_times

# Define sample sizes and feature sizes
sample_sizes = [500,1000,1500,2000,2500]
feature_sizes = [10,30,50,70,90]


# Run experiments
# cpu_times, gpu_times = run_experiments(sample_sizes, feature_sizes)

# np.save('cpu_times.npy', cpu_times)
# np.save('gpu_times.npy', gpu_times)

cpu_times = np.load('cpu_times.npy')[:,0:3]
gpu_times = np.load('gpu_times.npy')[:,0:3]


# Plot the results as heatmaps with shared colorbar
fig, axes = plt.subplots(1, 2, figsize=(10, 8))
plt.rcParams.update({'font.size': 22})
sns.heatmap(cpu_times, annot=True, fmt=".2f", ax=axes[0], xticklabels=feature_sizes[0:3], yticklabels=sample_sizes, cmap="viridis", cbar = False, vmin = 0 , vmax = cpu_times.max())
axes[0].set_title("CPU ")
axes[0].set_xlabel("Number of Features")
axes[0].set_ylabel("Number of Samples")

sns.heatmap(gpu_times, annot=True, fmt=".2f", ax=axes[1], xticklabels=feature_sizes[0:3], yticklabels=[], cmap="viridis", vmin = 0, vmax = cpu_times.max())
axes[1].set_title("GPU ")
axes[1].set_xlabel("Number of Features")

# Ensure the colorbar is consistent between subplots
cbar = axes[1].collections[0].colorbar
cbar.set_label("Training Time (seconds)")

plt.tight_layout()
plt.savefig('comp_time_heatmap.png')