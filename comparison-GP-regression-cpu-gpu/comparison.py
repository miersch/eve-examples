import os
import sys
import numpy as np
import torch
import gpytorch
import time
import math
import submitit
from datetime import datetime


def gpr_test(train_x, train_y) -> None:
    # We will use the simplest form of GP model, exact inference
    class ExactGPModel(gpytorch.models.ExactGP):
        def __init__(self, train_x, train_y, likelihood):
            super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
            self.mean_module = gpytorch.means.ConstantMean()
            self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())

        def forward(self, x):
            mean_x = self.mean_module(x)
            covar_x = self.covar_module(x)
            return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

    # initialize likelihood and model
    likelihood = gpytorch.likelihoods.GaussianLikelihood()
    model = ExactGPModel(train_x, train_y, likelihood)

    training_iter = 100


    # Find optimal model hyperparameters
    model.train()
    likelihood.train()

    # Use the adam optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters

    # "Loss" for GPs - the marginal log likelihood
    mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)

    start_time = start_time = datetime.now()
    for i in range(training_iter):
        # Zero gradients from previous iteration
        optimizer.zero_grad()
        # Output from model
        output = model(train_x)
        # Calc loss and backprop gradients
        loss = -mll(output, train_y)
        loss.backward()
        print('Iter %d/%d - Loss: %.3f   lengthscale: %.3f   noise: %.3f' % (
            i + 1, training_iter, loss.item(),
            model.covar_module.base_kernel.lengthscale.item(),
            model.likelihood.noise.item()
        ))
        optimizer.step()
    end_time = datetime.now()
    elapsed_time = end_time - start_time
    print(start_time)
    print(end_time)
    print(f"The training loop took {elapsed_time.total_seconds()} seconds to execute.")

    # model(train_x)

    test_x = torch.linspace(0, 1, 1000)
    f_preds = model(test_x)
    y_preds = likelihood(model(test_x))

    f_mean = f_preds.mean
    f_var = f_preds.variance
    f_covar = f_preds.covariance_matrix
    torch.Size([1000])
    f_samples = f_preds.sample(sample_shape=torch.Size([1000]))


    # Get into evaluation (predictive posterior) mode
    model.eval()
    likelihood.eval()

    # Test points are regularly spaced along [0,1]
    # Make predictions by feeding model through likelihood
    with torch.no_grad(), gpytorch.settings.fast_pred_var():
        test_x = torch.linspace(0, 1, 51)
        observed_pred = likelihood(model(test_x))
    # print(model)

    return elapsed_time



def main():
    work_folder = "/work/miersch/submitit_tests"

    # gpytorch training test with submission to gpu enabled node
    # Training data is 100 points in [0,1] inclusive regularly spaced
    train_x = torch.linspace(0, 1, 1000)
    # True function is sin(2*pi*x) with Gaussian noise
    train_y = torch.sin(train_x * (2 * math.pi)) + torch.randn(train_x.size()) * math.sqrt(0.04)
    # executor is the submission interface (logs are dumped in the folder)
    executor = submitit.AutoExecutor(folder=work_folder)
    # set timeout in min, and memory for running the job, request a gpu
    # executor.update_parameters(timeout_min=2, slurm_mem_per_cpu='8G', cpus_per_task=1, gpus_per_node = 1)
    executor.update_parameters(timeout_min=2, slurm_mem_per_cpu='1G', cpus_per_task=1)
    # run all jobs in parrallel as array jobs
    job = executor.submit(gpr_test, train_x, train_y)
    print(job.job_id)  # ID of your job
    output = job.result()
    # stich together the results into a single Dataset
    print(output.total_seconds())
    return


if __name__ == '__main__':
    main()
