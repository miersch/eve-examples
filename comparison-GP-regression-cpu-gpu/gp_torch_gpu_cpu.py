import torch
from sklearn.datasets import make_regression
import gpytorch
from gpytorch.models import ExactGP
from gpytorch.likelihoods import GaussianLikelihood
import pytorch_lightning as pl
from sklearn.metrics import r2_score
import submitit
import math
import matplotlib.pyplot as plt
import time


class GPModel(ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(GPModel, self).__init__(train_x, train_y, GaussianLikelihood())
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel(2))

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)
        
class GPModelLightning(pl.LightningModule):
    def __init__(self, train_x, train_y):
        super(GPModelLightning, self).__init__()
        self.likelihood = gpytorch.likelihoods.GaussianLikelihood()
        self.gp_model = GPModel(train_x, train_y, self.likelihood)
        self.mll = gpytorch.mlls.ExactMarginalLogLikelihood(self.likelihood, self.gp_model)
    def forward(self, x):
        return self.gp_model(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        output = self.gp_model(x)
        loss = -self.mll(output, y)
        self.log('train_loss', loss)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=0.1)
        return optimizer

    def train_dataloader(self):
        # return torch.utils.data.DataLoader(torch.utils.data.TensorDataset(self.train_x, self.train_y), batch_size=32)
        train_dataset = torch.utils.data.TensorDataset(self.gp_model.train_inputs[0], self.gp_model.train_targets)
        return torch.utils.data.DataLoader(train_dataset, batch_size=len(train_dataset))

def test_trainer(accelarator):
    # Generate toy dataset
    # train_x = torch.linspace(0, 1, 10000)
    # train_y = torch.sin(train_x * (2 * math.pi)) + torch.randn(train_x.size()) * math.sqrt(0.04)


    # Set seed for reproducibility
    torch.manual_seed(0)

    # Number of samples and features
    num_samples = 1000
    num_features = 2
    # Generate random data for features (x) - using a normal distribution
    train_x = torch.randn(num_samples, num_features)

    # Define coefficients for the linear relationship
    coefficients = torch.tensor([2.0, -3.0])

    # Generate target variable (y) with some added noise
    noise = torch.randn(num_samples) * 0.5
    train_y = train_x @ coefficients + noise  # @ is the matrix multiplication operator

    # Reshape train_y to be a column vector
    train_y = train_y.view(num_samples, 1)

    # Create GP model and trainer
    model = GPModelLightning(train_x, train_y)
    trainer = pl.Trainer(max_epochs=100, accelerator=accelarator, devices=1)

    # Train the model
    start_time = time.time()
    trainer.fit(model)
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f'Training took {elapsed_time}s')

    # Generate test dataset
    test_x = torch.linspace(0, 1.5, 150)
    test_y = torch.sin(test_x * (2 * math.pi)) + torch.randn(test_x.size()) * math.sqrt(0.04)

    # Make predictions on test and training set
    model.eval()
    with gpytorch.settings.fast_pred_var():
        train_pred = model.likelihood(model(train_x))
        test_pred = model.likelihood(model(test_x))

    # Calculate R-squared value
    with torch.no_grad():
        r2_test = r2_score(test_y, test_pred.mean.numpy())
        r2_train = r2_score(train_y, train_pred.mean.numpy())

    print("R-squared on test set:", r2_test)
    print("R-squared on training set:", r2_train)
    return elapsed_time

def main():
    # time_gpu = test_trainer(accelarator='gpu')
    time_gpu = 0
    time_cpu = test_trainer(accelarator='cpu')
    print(f'training took GPU:{time_gpu}, CPU:{time_cpu}')

if __name__ == "__main__":
    main()
