import numpy as np

# Example data (replace with your actual arrays)
cpu_times = np.array([[0.1, 0.2, 0.3], [0.4, 0.5, 0.6]])
gpu_times = np.array([[0.7, 0.8, 0.9], [1.0, 1.1, 1.2]])

# Save arrays to a file
filename = 'times_data.txt'
np.savetxt(filename, cpu_times, delimiter=',')
# Append the second array to the same file
with open(filename, 'ab') as f:  # 'ab' stands for append binary mode
    np.savetxt(f, gpu_times, delimiter=',')

# Read arrays from the file
with open(filename, 'rb') as f:  # 'rb' stands for read binary mode
    loaded_cpu_times = np.loadtxt(f, delimiter=',')
    loaded_gpu_times = np.loadtxt(f, delimiter=',')

# Print loaded arrays
print("CPU Times:")
print(loaded_cpu_times)
print("\nGPU Times:")
print(loaded_gpu_times)