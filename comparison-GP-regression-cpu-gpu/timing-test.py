import time
import submitit

def waiting():
    start_time = time.time()
    for i in range(10):
        time.sleep(0.1)
    end_time  = time.time()
    elapsed_time = end_time - start_time
    print(f'execution took {elapsed_time}s')
    return elapsed_time


def main():
    work_folder = "/work/miersch/submitit_tests"
    executor = submitit.AutoExecutor(folder=work_folder)
    # set timeout in min, and memory for running the job, request a gpu
    executor.update_parameters(timeout_min=2, slurm_mem_per_cpu='1G', cpus_per_task=1, gpus_per_node = 1)
    # run all jobs in parrallel as array jobs
    job = executor.submit(waiting)
    print(job.job_id)  # ID of your job
    output = job.result()
    # stich together the results into a single Dataset
    print(output)
    return

if __name__ == '__main__':
    main()